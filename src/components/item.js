import React from 'react'

export default ({ node }) => (
  <div className="column is-one-quarter">
    <div className="card item">
      <div className="card-image">
        <figure className="image is-3by2">
          <img src={node.image.file.url} />
        </figure>
      </div>
      <div className="card-content">
        <p className="title is-4">{node.title}</p>
        <p className="subtitle is-6">{node.description}</p>
      </div>
      <div className="card-footer">
        <a href="#" className="card-footer-item button is-primary">Order</a>
      </div>
    </div>
  </div>
)