import React from "react"
import Link from 'gatsby-link'
import '../styles/main.scss'

export default ({ children, data }) => {
  return (
    <div>
      <header className="hero-bg is-large">
      <div className="container is-fluid">
      <nav className="navbar is-transparent">
        <div className="navbar-brand">
          <div className="navbar-item">
            <Link className="has-text-white has-text-weight-bold" to='/'>{data.site.siteMetadata.title}</Link>
          </div>
        </div>
        <div className="navbar-menu is-centered">
            <Link className="navbar-item has-text-white" to='/'>Home</Link>
            <Link className="navbar-item has-text-white" to='/about/'>About</Link>
            <Link className="navbar-item has-text-white" to='/contact/'>Contact</Link>
        </div>
      </nav>
      </div>
        <div className="hero-body">
          <div className="container">
            <h1 className="title is-1  has-text-white">
              Primary bold title
            </h1>
            <h2 className="subtitle is-3 has-text-white">
              Primary bold subtitle
            </h2>
          </div>
        </div>
      </header>
      <div>
        {children()}
      </div>
    </div>
  );
}

export const pageQuery = graphql`
  query pageQuery {
    site {
      siteMetadata{
        title
      }
    }
  }
`