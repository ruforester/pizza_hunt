import React from "react"
import Item from '../components/item'

export default ({ data }) => {
  const edges = data.allContentfulItem.edges
  return (
    <section className="section">
      <div className="columns is-multiline">
        {
          edges.map(({ node }) => <Item node={node} key={node.id} />)
        }
      </div>
    </section>
  )
};

export const query = graphql`
query allItemsQuery {
  allContentfulItem {
    edges {
      node {
        id
        title
        description
        ingredients
        diameter
        weight
        price
        rating
        image {
          file {
            url
          }
        }
      }
    }
  }
}
`
