module.exports = {
  siteMetadata: {
    title: `Pizza Hunt`,
  },
  pathPrefix: '/pizza_hunt',
  plugins: [
    `gatsby-plugin-sass`,
    {
      resolve: `gatsby-source-contentful`,
      options: {
        spaceId: `hirwil4yo3yo`,
        accessToken: `27830dd065a0b36feb7885ec31928dead327579bf0c6d89883c4e21682a5c3e5`,
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: "Pizza Hunt",
        short_name: "Pizza Hunt",
        start_url: "/pizza_hunt/index.html",
        background_color: "#f7f0eb",
        theme_color: "#00D1B2",
        display: "standalone",
        icons: [
          {
            // Everything in /static will be copied to an equivalent
            // directory in /public during development and build, so
            // assuming your favicons are in /static/favicons,
            // you can reference them here
            src: `/pizza_hunt/favicons/android-chrome-192x192.png`,
            sizes: `192x192`,
            type: `image/png`,
          },
          {
            src: `/pizza_hunt/favicons/android-chrome-512x512.png`,
            sizes: `512x512`,
            type: `image/png`,
          },
        ],
      },
    },
    `gatsby-plugin-offline`
  ],
}